from django.conf.urls import url,include
from . import views
app_name='tcg'
urlpatterns = [
    url('^tcg/$',views.tcg,name="tcg1"),
    url('^tcg/numbers/$',views.number,name="numbers"),
    url('^tcg/matrix/$',views.matrixgen,name="matrix"),
    url('^tcg/array/$',views.arraygen,name="array"),
    url('^tcg/string/$',views.stringgen,name="string"),
    # url('^tcg/static/tcg/lifecoder_1/matrix.txt/$',views.abc,name="abc")


    ]