import string
import random

def id_generator(size, chars):
        lo=string.ascii_lowercase
        up=string.ascii_uppercase
        di=string.digits
        return ''.join(random.choice(chars) for _ in range(size))


def generate(tc,si,cha):
        if tc>=1000000:
                tc=random.randint(0,99999)
        open("string.txt", "w").close()
        file = open('string.txt','a')
        for i in range(tc):
                file.write(str(id_generator(si,cha))+'\n')
        file.close()

generate(3,5,'abcdefghijklmnopq')


