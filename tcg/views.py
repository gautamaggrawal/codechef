from django.shortcuts import render
from .forms import NumberForm,MatrixForm,ArrayForm,StringForm
from tcg.alltc import test1
from tcg.alltc import numbers,array,matrix
from django.shortcuts import redirect

# Create your views here.
from django.http import HttpResponse
from wsgiref.util import FileWrapper
import os


def number(request):
    form1=MatrixForm()
    form2=ArrayForm()
    form3=StringForm()

    if request.method == 'POST':
        #print(request.POST)
        form = NumberForm(request.POST)


        if form.is_valid():
            usern=str(request.user)
            tc=form.cleaned_data.get('testcase')
            minv=form.cleaned_data.get('minv')
            maxv=form.cleaned_data.get('maxv')
            numbers.generate(tc,minv,maxv,usern)
            return redirect('/static/tcg/'+usern+'/numbers.txt')

    else:
            form = NumberForm()
    return render(request, 'tcg/base.html', {'form':form,'form1':form1,'form2':form2,'form3':form3,'usern':usern})
    
def matrixgen(request):
    form=NumberForm()
    form2=ArrayForm()
    form3=StringForm()

    if request.method == 'POST':
        #print(request.POST)
        form1 = MatrixForm(request.POST)


        if form1.is_valid():
            usern=str(request.user)
            print(usern)
            tc=form1.cleaned_data.get('Testcase')
            minv=form1.cleaned_data.get('Minv')
            maxv=form1.cleaned_data.get('Maxv')
            unique=form1.cleaned_data.get('unique')
            rows=form1.cleaned_data.get('rows')
            col=form1.cleaned_data.get('col')
            matrix.generate(int(tc),int(rows),col,minv,maxv,unique,usern)
            return redirect('/static/tcg/'+usern+'/matrix.txt')
            

    else:
            form1 = MatrixForm()
    return render(request, 'tcg/base.html', {'form':form,'form1':form1,'form2':form2,'form3':form3,'usern':usern})


def arraygen(request):
    form=NumberForm()
    form1=MatrixForm()
    form3=StringForm()

    if request.method == 'POST':
        #print(request.POST)
        form2 = ArrayForm(request.POST)


        if form2.is_valid():
            usern=str(request.user)
            tc=form2.cleaned_data.get('Testcase')
            minv=form2.cleaned_data.get('minin')
            maxv=form2.cleaned_data.get('maxin')
            unique=form2.cleaned_data.get('unique')
            arrsize=form2.cleaned_data.get('arrsize')
            print(tc,minv,maxv,unique,arrsize)
            array.generate(int(tc),arrsize,minv,maxv,unique,usern)
            return redirect('/static/tcg/'+usern+'/array.txt')


    else:
            form2 = ArrayForm()
    return render(request, 'tcg/base.html', {'form':form,'form1':form1,'form2':form2,'form3':form3,'usern':usern})

def stringgen(request):
    form=NumberForm()
    form1=MatrixForm()
    form2=ArrayForm()

    
    if request.method == 'POST':
        #print(request.POST)
        form3=StringForm(request.POST)


        if form3.is_valid():
            pass

    else:
            form3=StringForm()
    return render(request, 'tcg/base.html', {'form':form,'form1':form1,'form2':form2,'form3':form3})






def tcg(request):

    form=NumberForm()
    form1=MatrixForm()
    form2=ArrayForm()
    form3=StringForm()
    usern=str(request.user)

    return render(request, 'tcg/base.html', {'form':form,'form1':form1,'form2':form2,'form3':form3,'usern':usern})

#def tcg(request):
	#return render(request, )