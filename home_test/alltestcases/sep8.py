from test1 import Rand,unique
import random
import os

def generate(tc,n,q,arrmax):
    if tc>1000:
        tc=random.randint(1,1000)
    if n>100000:
        n=random.randint(1,100000)
    if q>500000:
        q=random.randint(1,5*(10**5))
    open("arraygen.txt", "w").close()
    file = open('arraygen.txt','a')
    file.write(str(tc)+'\n')
    for i in range(0,tc):
        n=random.randint(1,n)
        q=random.randint(1,q)
        file.write(str(n)+' '+str(q)+'\n')
        list1=Rand(1,arrmax,n)
        st=" ".join(str(x) for x in list1)
        file.write(st)
        file.write('\n')
        for i in range(0,q):
            l=random.randint(1,n)
            r=random.randint(1,n)
            file.write(str(l)+' '+str(r)+'\n')
    file.close()
    return(tc,tc,1)
        


    
