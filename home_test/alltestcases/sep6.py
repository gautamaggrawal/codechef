
from test1 import Rand,unique
import random
import os

def generate(tc,m,n,q):
    if n>100000:
        n=random.randint(1,9999)
    if m>=1000000:
        m=random.randint(1,9999)
    if q>=100000:
        k=random.randint(1,9999)
    open("arraygen.txt", "w").close()
    file = open('arraygen.txt','a')
    file.write(str(tc)+'\n')
    for i in range(tc):
        m=random.randint(1,m)
        ml=Rand(0,1,m)
        stm="".join(str(x) for x in ml)
        file.write(stm+'\n')
        n=random.randint(1,n)
        nl=Rand(0,1,n)
        stn="".join(str(x) for x in nl)
        print(nl)
        file.write(stn+'\n')
        q=random.randint(1,q)
        file.write(str(q)+'\n')
        for j in range(0,q):
            x=random.randint(1,n)
            y=random.randint(1,m)
            file.write(str(x)+' '+str(y)+'\n')
    file.close()
    return(tc,tc,1)

