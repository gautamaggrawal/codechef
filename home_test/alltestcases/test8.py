from test1 import unique,Rand
import random
def generate(p,s):
        if s>=31:
                s=random.randint(1,31)
        if p>=100001:
                p=random.randint(1,100001)
        open("arraygen.txt", "w").close()
        file = open('arraygen.txt','a')
        file.write(str(p)+" "+str(s)+'\n')
        for i in range(0,p):
                list1=(Rand(1,100,3))
                list2=(Rand(1,1000,3))
                finalans1=unique(list1,3,1,100)
                finalans2=unique(list2,3,1,1000)
                st1=" ".join(str(x) for x in finalans1)
                file.write(st1+'\n')
                st2=" ".join(str(x) for x in finalans2)
                file.write(st2+'\n')
        file.close()
        return(p,p,'null')
